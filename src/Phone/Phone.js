import React, { Component } from "react";
import Cart_Phone from "./Cart_Phone";
import { data_phone } from "./Data_Phone";
import Information from "./Information";
import List_Phone from "./List_Phone";

export default class Phone extends Component {
  state = {
    listPhone: data_phone,
    ttPhone: data_phone[0],
    cart: [],
  };
  changerPhone = (
    maSP,
    manHinh,
    heDieuHanh,
    cameraTruoc,
    cameraSau,
    ram,
    rom,
    hinhAnh,
    tenSP,
    giaBan
  ) => {
    this.setState({
      ttPhone: {
        maSP: maSP,
        manHinh: manHinh,
        heDieuHanh: heDieuHanh,
        cameraTruoc: cameraTruoc,
        cameraSau: cameraSau,
        ram: ram,
        rom: rom,
        hinhAnh: hinhAnh,
        tenSP: tenSP,
        giaBan: giaBan,
      },
    });
  };
  handleAddToCart = (phone) => {
    let newCart = [...this.state.cart];
    let index = newCart.findIndex((item) => {
      return item.maSP == phone.maSP;
    });
    if (index == -1) {
      let newPhone = { ...phone, soluong: 1 };
      newCart.push(newPhone);
    } else {
      newCart[index].soluong++;
    }
    this.setState({
      cart: newCart,
    });
  };
  handleChangerQuantity = (maSPphone, luachon) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.maSP == maSPphone;
    });
    cloneCart[index].soluong = cloneCart[index].soluong + luachon;
    if (cloneCart[index].soluong >= 1) {
      this.setState({
        cart: cloneCart,
      });
    } else {
      let newCart = this.state.cart.filter((item) => {
        return item.maSP != maSPphone;
      });
      this.setState({
        cart: newCart,
      });
    }
  };
  handleDelete = (maSPphone) => {
    let newCart = this.state.cart.filter((item) => {
      return item.maSP != maSPphone;
    });
    this.setState({
      cart: newCart,
    });
  };

  render() {
    return (
      <div className="container">
        <Cart_Phone
          cart={this.state.cart}
          changerQuantity={this.handleChangerQuantity}
          delete={this.handleDelete}
        />
        <List_Phone
          addtocart={this.handleAddToCart}
          listPhone={this.state.listPhone}
          changerPhone={this.changerPhone}
        />
        <Information ttPhone={this.state.ttPhone} />
      </div>
    );
  }
}
