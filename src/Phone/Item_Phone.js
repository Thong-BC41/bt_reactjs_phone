import React, { Component } from "react";

export default class Item_Phone extends Component {
  render() {
    let {
      maSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      hinhAnh,
      tenSP,
      giaBan,
    } = this.props.phone;
    return (
      <div className="col-4 p-4">
        <div className="card border-primary">
          <img className="card-img-top" src={hinhAnh} alt />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan}</p>
            <button
              onClick={() => {
                this.props.changerPhone(
                  maSP,
                  manHinh,
                  heDieuHanh,
                  cameraTruoc,
                  cameraSau,
                  ram,
                  rom,
                  hinhAnh,
                  tenSP,
                  giaBan
                );
              }}
              className="btn btn-success mx-3"
            >
              Xem chi tiết
            </button>
            <button
              onClick={() => {
                this.props.addtocart(this.props.phone);
              }}
              className="btn btn-warning"
            >
              Thêm giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}
