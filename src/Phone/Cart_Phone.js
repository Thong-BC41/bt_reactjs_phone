import React, { Component } from "react";

export default class Cart_Phone extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.maSP}</td>
          <td>{item.tenSP}</td>
          <td>
            <img style={{ width: 50 }} src={item.hinhAnh} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.changerQuantity(item.maSP, -1);
              }}
              className="btn btn-success mx-1"
            >
              -
            </button>
            <strong>{item.soluong}</strong>
            <button
              onClick={() => {
                this.props.changerQuantity(item.maSP, +1);
              }}
              className="btn btn-success mx-1"
            >
              +
            </button>
          </td>
          <td>{item.giaBan * item.soluong}</td>
          <td>
            <button
              onClick={() => {
                this.props.delete(item.maSP);
              }}
              className="btn btn-danger"
            >
              DELETE
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <h2>Cart_Phone</h2>
        <table className="table">
          <thead>
            <tr>
              <th>Mã SP</th>
              <th>Tên SP</th>
              <th>Hình ảnh</th>
              <th>Quantity</th>
              <th>Giá Bán</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
