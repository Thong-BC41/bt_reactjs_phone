import React, { Component } from "react";
import Item_Phone from "./Item_Phone";

export default class List_Phone extends Component {
  renderPhone = () => {
    return this.props.listPhone.map((item) => {
      return (
        <Item_Phone
          phone={item}
          addtocart={this.props.addtocart}
          changerPhone={this.props.changerPhone}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderPhone()}</div>;
  }
}
