import logo from "./logo.svg";
import "./App.css";
import Phone from "./Phone/Phone";

function App() {
  return (
    <div className="App">
      <Phone />
    </div>
  );
}

export default App;
